//
//  InterfaceController.swift
//  WatchKitDemo WatchKit Extension
//
//  Created by Navpreet Kaur on 2019-10-15.
//  Copyright © 2019 Navneet. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {
    
    var count = 0

    //Label for output
    @IBOutlet weak var outputLabel: WKInterfaceLabel!
    
    @IBOutlet weak var msgFromPhoneLabel: WKInterfaceLabel!
    
    @IBOutlet weak var nextPageLabel: WKInterfaceLabel!
    
    @IBAction func nextPageButton() {
    }
    //Do something when user presses the button
    
    @IBAction func watchButtonPressed() {
        print("I clicked the button")
        self.count = count + 1
    self.outputLabel.setText("Welcome!! \(count)")
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
}
