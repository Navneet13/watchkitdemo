//
//  ViewController.swift
//  WatchKitDemo
//
//  Created by Navpreet Kaur on 2019-10-15.
//  Copyright © 2019 Navneet. All rights reserved.
//

import UIKit
import WatchConnectivity  //Built-in Library for making phone <> Watch communication work

class ViewController: UIViewController, WCSessionDelegate {
    
    //Built-in methods for dealing with communication of phone & watch
    //----------------------------------------------------------------
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    // MARK: Outlets
       // ------------------------
       @IBOutlet weak var counterLabel: UILabel!
       @IBOutlet weak var anotherOutputLabel: UILabel!
       
       // MARK: Variables
       // ------------------------
       var phoneCounter:Int = 0
       var messageCounter = 0
       
       override func viewDidLoad() {
           super.viewDidLoad()
           print("---PHONE APP LOADED!")
       }

    
    // MARK: Custom Functions
       // ------------------------
       @IBAction func phoneButtonPressed(_ sender: UIButton) {
           
           print("PHONE BUTTON PRESSED")
           self.phoneCounter = self.phoneCounter + 1;
           self.counterLabel.text = "Counter: \(self.phoneCounter)"
       }
       
    
     @IBAction func sendMessageButton(_ sender: Any) {
     print("Sending the message to the watch")
        messageCounter = messageCounter + 1
        anotherOutputLabel.text = "Message Sent \(messageCounter)"
     
     }
    
  
    
    
}

